const nav = document.querySelector("#nav");
const menu = document.querySelector("#menu");
const menuToggle = document.querySelector(".nav__toggle");
let isMenuOpen = false;

// TOGGLE MENU ACTIVE STATE
menuToggle.addEventListener("click", (e) => {
  e.preventDefault();
  isMenuOpen = !isMenuOpen;

  // toggle a11y attributes and active class
  menuToggle.setAttribute("aria-expanded", String(isMenuOpen));
  menu.hidden = !isMenuOpen;
  nav.classList.toggle("nav--open");
});

// TRAP TAB INSIDE NAV WHEN OPEN
nav.addEventListener("keydown", (e) => {
  // abort if menu isn't open or modifier keys are pressed
  if (!isMenuOpen || e.ctrlKey || e.metaKey || e.altKey) {
    return;
  }

  // listen for tab press and move focus
  // if we're on either end of the navigation
  const menuLinks = menu.querySelectorAll(".nav__link");
  if (e.keyCode === 9) {
    if (e.shiftKey) {
      if (document.activeElement === menuLinks[0]) {
        menuToggle.focus();
        e.preventDefault();
      }
    } else if (document.activeElement === menuToggle) {
      menuLinks[0].focus();
      e.preventDefault();
    }
  }
});

const items = document.querySelectorAll(".cardMobile");
const itemCount = items.length;
const nextItem = document.querySelector(".next");
const previousItem = document.querySelector(".previous");
let count = 0;

function showNextItem() {
  items[count].classList.remove("active");

  if (count < itemCount - 1) {
    count++;
  } else {
    count = 0;
  }

  items[count].classList.add("active");
  console.log(count);
}

function showPreviousItem() {
  items[count].classList.remove("active");

  if (count > 0) {
    count--;
  } else {
    count = itemCount - 1;
  }

  items[count].classList.add("active");
  console.log(count);
}

function keyPress(e) {
  e = e || window.event;

  if (e.keyCode == "37") {
    showPreviousItem();
  } else if (e.keyCode == "39") {
    showNextItem();
  }
}

nextItem.addEventListener("click", showNextItem);
previousItem.addEventListener("click", showPreviousItem);

const scroll =
  window.requestAnimationFrame ||
  function (callback) {
    window.setTimeout(callback, 1000 / 60);
  };

const elementsToShow = document.querySelectorAll(".show");
const elementsToShowP = document.querySelectorAll(".show-p");
function loop() {
  elementsToShow.forEach(function (element) {
    if (isElementInViewport(element)) {
      element.classList.add("is-visible");
    }
  });
  elementsToShowP.forEach(function (element) {
    if (isElementInViewport(element)) {
      element.classList.add("is-visible");
    }
  });
  scroll(loop);
}

loop();

function isElementInViewport(el) {
  // special bonus for those using jQuery
  if (typeof jQuery === "function" && el instanceof jQuery) {
    el = el[0];
  }
  var rect = el.getBoundingClientRect();
  return (
    (rect.top <= 0 && rect.bottom >= 0) ||
    (rect.bottom >=
      (window.innerHeight || document.documentElement.clientHeight) &&
      rect.top <=
        (window.innerHeight || document.documentElement.clientHeight)) ||
    (rect.top >= 0 &&
      rect.bottom <=
        (window.innerHeight || document.documentElement.clientHeight))
  );
}

let prevScrollpos = window.pageYOffset;
window.onscroll = function () {
  let currentScrollPos = window.pageYOffset;

  if (prevScrollpos) {
    document.getElementById("scroll").style.top = "0";
    document.getElementById("navScroll").style.opacity = "0";
    document.getElementById("message-icon").style.opacity = "1";
    // document.getElementById("navScroll-mobile").style.opacity = "0";
    document.getElementById("logo-scroll").style.opacity = "0";
  }
  if (currentScrollPos === 0) {
    document.getElementById("scroll").style.top = "-130px";
    document.getElementById("navScroll").style.opacity = "1";
    // document.getElementById("scroll-mobile").style.top = "-130px";
    document.getElementById("message-icon").style.opacity = "0";
    document.getElementById("logo-scroll").style.opacity = "1";
  }

  prevScrollpos = currentScrollPos;
};
const messageToShowP = document.querySelector(".message-mobile");
messageToShowP.addEventListener("click", (event) => {
  window.open("contacto.html", "_self");
});
